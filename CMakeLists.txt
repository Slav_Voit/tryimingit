cmake_minimum_required(VERSION 3.15)
project(TheFirstCProject C)

set(CMAKE_C_STANDARD 90)

add_executable(TheFirstCProject main.c)